#include "gjson-cpp.h"
#include "gjson-parse.h"
#include <cstdio>
#include <iostream>

using namespace std;
using namespace json;

void ParseTest ()
{
	OwnedFileByteSource owned_file { "sample.json" };
	UTF8DecodingCharSource char_src { &owned_file };

    try
    {
        shared_ptr<Value> parsed = Parse(&char_src);
        cout << parsed << endl;
    }
    catch (BadSyntax bs)
    {
        cerr << "Syntax error on line " << bs.line << ", column " << bs.col << ": " << bs.why << endl;
        throw bs;
    }
}
