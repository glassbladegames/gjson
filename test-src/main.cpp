#include "gjson-cpp.h"
using namespace json;

#include <iostream>
using namespace std;

struct ShouldHaveThrown {};
struct BadResult {};

void ParseTest ();

int main ()
{
    cout << "tests running" << endl;

    shared_ptr<Value> root_obj {new Obj};
    {
        root_obj->Set("some_id", shared_ptr<Value> {new Int {10}});

        shared_ptr<Value> arr {new Arr};
        root_obj->Set("an_array", arr);
        arr->Set(0, shared_ptr<Value> {new Bool {true}});
        arr->Set(1, shared_ptr<Value> {new Str {"hello"}});
        arr->Set(1, shared_ptr<Value> {new Str {"hey there!"}});
        try
        {
            arr->Set(3, shared_ptr<Value> { new Str {"shouldn't be written"}});
            throw ShouldHaveThrown {};
        }
        catch (OutOfRange) {}
        try
        {
            root_obj->Len();
            throw ShouldHaveThrown {};
        }
        catch (TypeMismatch) {}
        if (arr->Len() != 2) throw BadResult {};
        root_obj->Set("null", shared_ptr<Value> {new Null});
        root_obj->Set("bool", shared_ptr<Value> {new Bool {false}});
        root_obj->Set("bool", shared_ptr<Value> {new Bool {true}});
        try
        {
            arr->Get(2);
            throw ShouldHaveThrown {};
        }
        catch (OutOfRange) {}
        try
        {
            root_obj->Get("key that doesn't exist");
            throw ShouldHaveThrown {};
        }
        catch (OutOfRange) {}
        try
        {
            root_obj->Get("bool")->AsInt();
            throw ShouldHaveThrown {};
        }
        catch (TypeMismatch) {}
        root_obj->Get("bool")->AsBool();
        root_obj->Get("bool")->AsStr();
        try
        {
            arr->Get(0)->AsInt();
            throw ShouldHaveThrown {};
        }
        catch (TypeMismatch) {}
        arr->Get(0)->Type();
        arr->Get(0)->AsStr();
    }

    cout << root_obj << endl;

	ParseTest();

    {
        cout << "try operator[]" << endl;
        shared_ptr<Value> top = shared_ptr<Value> { new Obj };
        (*top)["a"] = shared_ptr<Value> { new Int { 5 } };
        cout << top << endl;
        (*top)["arr"] = shared_ptr<Value> { new Arr };
        (*(*top)["arr"])[0] = shared_ptr<Value> { new Int { 30 } };
        cout << top << endl;
    }
    {
        auto top = Obj::New();
        (*top)["a"] = Int::New(5);
        cout << top << endl;
        (*top)["arr"] = Arr::New();
        (*(*top)["arr"])[0] = Int::New(30);
        cout << top << endl;
        auto arr = (*top)["arr"];
        (*arr)[arr->Len()] = Int::New(-1);
        cout << top << endl;

        cout << top->Has("a") << ","
             << top->Has("arr") << ","
             << top->Has("asdf") << endl;
    }

    return 0;
}
