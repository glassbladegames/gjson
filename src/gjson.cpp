#include "gjson-cpp.h"

#include <sstream>
using namespace std;

namespace json
{
    bool Value::AsBool ()
    {
        throw TypeMismatch {};
    }
    INT_TYPE Value::AsInt ()
    {
        throw TypeMismatch {};
    }
    FLOAT_TYPE Value::AsFloat ()
    {
        throw TypeMismatch {};
    }
    shared_ptr<Value> Value::Get (size_t index)
    {
        throw TypeMismatch {};
    }
    INT_TYPE Value::Len ()
    {
        throw TypeMismatch {};
    }
    vector<string> Value::Keys ()
    {
        throw TypeMismatch {};
    }
    bool Value::Has (string key)
    {
        throw TypeMismatch {};
    }
    shared_ptr<Value> Value::Get (string key)
    {
        throw TypeMismatch {};
    }
    void Value::Set (size_t index, shared_ptr<Value> to)
    {
        throw TypeMismatch {};
    }
    void Value::Set (string key, shared_ptr<Value> to)
    {
        throw TypeMismatch {};
    }
    std::shared_ptr<Value>& Value::operator[] (size_t index)
    {
        throw TypeMismatch {};
    }
    std::shared_ptr<Value>& Value::operator[] (std::string key)
    {
        throw TypeMismatch {};
    }

    std::ostream& operator<< (std::ostream& stream, std::shared_ptr<Value> value)
    {
        value->Serialize(stream);
        return stream;
    }

    ValueType Null::Type ()
    {
        return TYPE_NULL;
    }
    string Null::AsStr ()
    {
        return "null";
    }
    void Null::Serialize (ostream& stream)
    {
        stream << AsStr();
    }
    shared_ptr<Value> Null::New ()
    {
        return shared_ptr<Value> { new Null };
    };

    ValueType Bool::Type ()
    {
        return TYPE_BOOL;
    }
    Bool::Bool (bool value) : value(value) {}
    bool Bool::AsBool ()
    {
        return value;
    }
    string Bool::AsStr ()
    {
        return value ? "true" : "false";
    }
    void Bool::Serialize (ostream& stream)
    {
        stream << AsStr();
    }
    shared_ptr<Value> Bool::New (bool value)
    {
        return shared_ptr<Value> { new Bool { value } };
    };

    ValueType Int::Type ()
    {
        return TYPE_INT;
    }
    Int::Int (INT_TYPE value) : value(value) {}
    INT_TYPE Int::AsInt ()
    {
        return value;
    }
    FLOAT_TYPE Int::AsFloat ()
    {
        return value;
    }
    string Int::AsStr ()
    {
        ostringstream out;
        Serialize(out);
        return out.str();
    }
    void Int::Serialize (ostream& stream)
    {
        stream << value;
    }
    shared_ptr<Value> Int::New (INT_TYPE value)
    {
        return shared_ptr<Value> { new Int { value } };
    };

    ValueType Float::Type ()
    {
        return TYPE_FLOAT;
    }
    Float::Float (FLOAT_TYPE value) : value(value) {}
    INT_TYPE Float::AsInt ()
    {
        return value;
    }
    FLOAT_TYPE Float::AsFloat ()
    {
        return value;
    }
    string Float::AsStr ()
    {
        ostringstream out;
        Serialize(out);
        return out.str();
    }
    void Float::Serialize (ostream& stream)
    {
        stream << value;
    }
    shared_ptr<Value> Float::New (FLOAT_TYPE value)
    {
        return shared_ptr<Value> { new Float { value } };
    };

    ValueType Str::Type ()
    {
        return TYPE_STR;
    }
    Str::Str (string value) : value(value) {}
    string Str::AsStr ()
    {
        return value;
    }
    void Str::Serialize (ostream& stream)
    {
        stream << '"' << value << '"';
    }
    shared_ptr<Value> Str::New (string value)
    {
        return shared_ptr<Value> { new Str { value } };
    };

    ValueType Arr::Type ()
    {
        return TYPE_ARR;
    }
    string Arr::AsStr ()
    {
        ostringstream out;
        Serialize(out);
        return out.str();
    }
    shared_ptr<Value> Arr::Get (size_t index)
    {
        if (index >= values.size()) throw OutOfRange {};
        return values[index];
    }
    INT_TYPE Arr::Len ()
    {
        return values.size();
    }
    void Arr::Set (size_t index, shared_ptr<Value> value)
    {
        if (index > values.size())
        {
            throw OutOfRange {};
        }
        else if (index < values.size())
        {
            values[index] = value;
        }
        else
        {
            values.push_back(value);
        }
    }
    void Arr::Serialize (ostream& out)
    {
        out << "[";
        bool first = true;
        for (shared_ptr<Value> value : values)
        {
            if (first) first = false;
            else out << ",";
            out << " " << value;
        }
        out << " ]";
    }
    std::shared_ptr<Value>& Arr::operator[] (size_t index)
    {
        if (index == values.size())
        {
            values.push_back(shared_ptr<Value> { new Null });
        }
        if (index < values.size())
        {
            return values[index];
        }
        throw OutOfRange {};
    }
    shared_ptr<Value> Arr::New ()
    {
        return shared_ptr<Value> { new Arr };
    };

    ValueType Obj::Type ()
    {
        return TYPE_OBJ;
    }
    string Obj::AsStr ()
    {
        ostringstream out;
        Serialize(out);
        return out.str();
    }
    shared_ptr<Value> Obj::Get (string key)
    {
        auto found = values.find(key);
        if (found == values.end())
        {
            throw OutOfRange {};
        }
        return found->second;
    }
    vector<string> Obj::Keys ()
    {
        vector<string> keys;
        for (pair<string, shared_ptr<Value>> pair : values)
        {
            keys.push_back(pair.first);
        }
        return keys;
    }
    bool Obj::Has (string key)
    {
        return values.count(key);
    }
    void Obj::Set (string key, shared_ptr<Value> value)
    {
        values[key] = value;
    }
    void Obj::Serialize (ostream& out)
    {
        out << "{";
        bool first = true;
        for (pair<string, shared_ptr<Value>> pair : values)
        {
            if (first) first = false;
            else out << ",";
            out << " \"" << pair.first << "\": " << pair.second;
        }
        out << " }";
    }
    std::shared_ptr<Value>& Obj::operator[] (std::string key)
    {
        if (!Has(key))
        {
            values[key] = Null::New();
        }
        return values[key];
    }
    shared_ptr<Value> Obj::New ()
    {
        return shared_ptr<Value> { new Obj };
    };
}
