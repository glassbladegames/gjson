#include "gjson-cpp.h"
#include "gjson-parse.h"

#include <cmath>
#include <sstream>

using namespace std;

namespace json
{
    struct LineColTracker : public CharSource
    {
        int line = 1, col = 1;
        int prior = EOF;
        CharSource* internal;
        LineColTracker (CharSource* internal) : internal(internal) {}
        int ReadChar () override
        {
            if (prior == '\n')
            {
                line++;
                col = 1;
            }
            else if (prior != EOF)
            {
                col++;
            }

            prior = internal->ReadChar();
            return prior;
        }
    };

    struct Parser
    {
        LineColTracker src;
        int nextch;
        Parser (CharSource* src)
            : src(src)
        {
            Advance();
        }
        void Advance ()
        {
            nextch = src.ReadChar();
        }
        void DiscardWhitespace ()
        {
            while (isspace(nextch)) Advance();
        }
        INT_TYPE ReadInt (int& places)
        {
            places = 0;
            INT_TYPE accumulated = 0;
            while (isdigit(nextch))
            {
                accumulated *= 10;
                accumulated += nextch - '0';
                Advance();
                places++;
            }
            return accumulated;
        }
        shared_ptr<Value> ReadNumeric ()
        {
            bool negative = nextch == '-';
            if (negative) Advance();

            int places;
            INT_TYPE base = ReadInt(places);

            if (nextch == '.')
            {
                Advance();

                INT_TYPE after_dot_int = ReadInt(places);
                FLOAT_TYPE after_dot_scaled = after_dot_int * pow(0.1, places);
                FLOAT_TYPE combined = base + after_dot_scaled;

                if (negative) combined = -combined;
                return shared_ptr<Value> { new Float { combined } };
            }
            else
            {
                if (negative) base = -base;
                return shared_ptr<Value> { new Int { base } };
            }
        }
        string ReadQuotedStr ()
        {
            Advance();

            ostringstream out;
            while (true)
            {
                if (nextch == EOF) throw BadSyntax {src.line, src.col, "unterminated quoted string"};

                if (nextch == '"')
                {
                    Advance();
                    return out.str();
                }

                // We are NOT BEING UNICODE FRIENDLY with this!!
                // TO DO: Either re-encode as UTF8 or make strings of codepoint
                // integers.
                out << (char)nextch;
                Advance();
            }
        }
        shared_ptr<Value> ReadObj ()
        {
            Advance();

            shared_ptr<Value> obj { new Obj };

            DiscardWhitespace();
            if (nextch == '}')
            {
                Advance();
                return obj;
            }

            while (true)
            {
                DiscardWhitespace();
                if (nextch != '"') throw BadSyntax {src.line, src.col, "expected '\"'"};

                string key = ReadQuotedStr();

                DiscardWhitespace();
                if (nextch != ':') throw BadSyntax {src.line, src.col, "expected ':'"};
                Advance();

                shared_ptr<Value> value = ReadValue();

                obj->Set(key, value);

                DiscardWhitespace();
                if (nextch == '}')
                {
                    Advance();
                    return obj;
                }
                else if (nextch != ',') throw BadSyntax {src.line, src.col, "expected '}' or ','"};
                Advance();
            }
        }
        shared_ptr<Value> ReadArr ()
        {
            Advance();

            shared_ptr<Value> arr { new Arr };

            DiscardWhitespace();
            if (nextch == ']')
            {
                Advance();
                return arr;
            }

            int index = 0;
            while (true)
            {
                shared_ptr<Value> value = ReadValue();

                arr->Set(index++, value);

                DiscardWhitespace();
                if (nextch == ']')
                {
                    Advance();
                    return arr;
                }
                else if (nextch != ',') throw BadSyntax {src.line, src.col, "expected ']' or ','"};
                Advance();
            }
        }
        shared_ptr<Value> ReadKeyword ()
        {
            string keyword;
            while (true)
            {
                keyword += nextch;
                Advance();
                switch (nextch)
                {
                    case 'a' ... 'z': case 'A' ... 'Z':
                    if (keyword.length() < 5) continue;
                }

                if (keyword == "null") return shared_ptr<Value> { new Null };
                if (keyword == "true") return shared_ptr<Value> { new Bool { true } };
                if (keyword == "false") return shared_ptr<Value> { new Bool { false } };
                throw BadSyntax {src.line, src.col, "invalid keyword"};
            }
        }
        shared_ptr<Value> ReadValue ()
        {
            DiscardWhitespace();

            switch (nextch)
            {
                case '0' ... '9':
                case '-':
                case '.':
                return ReadNumeric();

                case '"':
                return shared_ptr<Value> { new Str { ReadQuotedStr() } };

                case '{':
                return ReadObj();

                case '[':
                return ReadArr();

                case 'a' ... 'z': case 'A' ... 'Z':
                return ReadKeyword();

                default:
                throw BadSyntax {src.line, src.col, "invalid character to begin a value"};
            }
        }
    };

	shared_ptr<Value> Parse (CharSource* char_src)
	{
        Parser parser { char_src };
        shared_ptr<Value> top_level = parser.ReadValue();

        // We expect exactly one value, and then maybe some whitespace but
        // otherwise the end of the source. So check the source for additional
        // content and if it exists throw a syntax error.
        parser.DiscardWhitespace();
        if (parser.nextch != EOF) throw BadSyntax {parser.src.line, parser.src.col, "unexpected extra content in file"};

        return top_level;
	}
}
