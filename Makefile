#default: clean test
default: lib/libgjsoncpp.a

INCLUDES += -Iinclude -Iparser-utils/include

ifeq ($(OPTIMIZATIONS),1)
CFLAGS += -fPIC -O2 -fomit-frame-pointer -s
else
CFLAGS += -fPIC -w -g
endif
CXXFLAGS += $(CFLAGS) -fpermissive

#OBJS = $(patsubst %.cpp,%.o,$(shell find src -iname '*.cpp'))
#TEST_OBJS = $(patsubst %.cpp,%.o,$(shell find test-src -iname '*.cpp'))

OBJS = $(patsubst %.cpp,%.o,$(shell find src -iname "*.cpp"))
OBJS += $(patsubst %.cpp,%.o,$(shell find parser-utils/src -iname "*.cpp"))
TEST_OBJS = $(patsubst %.cpp,%.o,$(shell find test-src -iname "*.cpp"))

test: bin/test
	"bin/test"

bin/test: lib/libgjsoncpp.a $(TEST_OBJS)
	mkdir -p bin
	$(CXX) $(CXXFLAGS) $(INCLUDES) $(TEST_OBJS) -Llib -lgjsoncpp -o $@

lib/libgjsoncpp.a: $(OBJS)
	mkdir -p lib
	ar -cr $@ $^

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $(INCLUDES) $< -o $@

clean:
	rm -f $(OBJS) $(TEST_OBJS)
	rm -rf lib
	rm -rf bin
