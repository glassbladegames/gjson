# GJSON

gjson (Glass Blade JSON):
JSON encoding/decoding library with C API and wrapping C++ API.
Strict syntax checking mode, as well as optional support for human-friendly syntax features such as comments.
