#pragma once

#include <cstdint>
#include <vector>
#include <unordered_map>
#include <string>
#include <ostream>
#include <memory>

namespace json
{
    typedef int64_t INT_TYPE;
    typedef double FLOAT_TYPE;

    enum ValueType
    {
        TYPE_NULL,
        TYPE_BOOL,
        TYPE_INT,
        TYPE_FLOAT,
        TYPE_STR,
        TYPE_ARR,
        TYPE_OBJ,
    };

    struct GeneralException {};
	struct AccessException : public GeneralException {};
    struct TypeMismatch : public AccessException {};
    struct OutOfRange : public AccessException {};

    struct Value
    {
        virtual ValueType Type () = 0;
        virtual bool AsBool ();
        virtual INT_TYPE AsInt ();
        virtual FLOAT_TYPE AsFloat ();
        virtual std::string AsStr () = 0;
        virtual std::shared_ptr<Value> Get (size_t index);
        virtual INT_TYPE Len ();
        virtual std::vector<std::string> Keys ();
        virtual bool Has (std::string key);
        virtual std::shared_ptr<Value> Get (std::string key);
        virtual void Set (size_t index, std::shared_ptr<Value> to);
        virtual void Set (std::string key, std::shared_ptr<Value> to);
        virtual void Serialize (std::ostream& stream) = 0;
        virtual std::shared_ptr<Value>& operator[] (size_t index);
        virtual std::shared_ptr<Value>& operator[] (std::string key);
    };

    std::ostream& operator<< (std::ostream& stream, std::shared_ptr<Value> value);

    struct Null : public Value
    {
        ValueType Type () override;
        std::string AsStr () override;
        void Serialize (std::ostream& stream) override;
        static std::shared_ptr<Value> New ();
    };
    struct Bool : public Value
    {
        ValueType Type () override;
        bool value;
        Bool (bool value);
        bool AsBool () override;
        std::string AsStr () override;
        void Serialize (std::ostream& stream) override;
        static std::shared_ptr<Value> New (bool value);
    };
    struct Int : public Value
    {
        ValueType Type () override;
        INT_TYPE value;
        Int (INT_TYPE value);
        INT_TYPE AsInt () override;
        FLOAT_TYPE AsFloat ();
        std::string AsStr ();
        void Serialize (std::ostream& stream) override;
        static std::shared_ptr<Value> New (INT_TYPE value);
    };
    struct Float : public Value
    {
        ValueType Type () override;
        FLOAT_TYPE value;
        Float (FLOAT_TYPE value);
        INT_TYPE AsInt ();
        FLOAT_TYPE AsFloat ();
        std::string AsStr () override;
        void Serialize (std::ostream& stream) override;
        static std::shared_ptr<Value> New (FLOAT_TYPE value);
    };
    struct Str : public Value
    {
        ValueType Type () override;
        std::string value;
        Str (std::string value);
        std::string AsStr () override;
        void Serialize (std::ostream& stream) override;
        static std::shared_ptr<Value> New (std::string value);
    };
    struct Arr : public Value
    {
        ValueType Type () override;
        std::vector<std::shared_ptr<Value>> values;
        std::string AsStr () override;
        std::shared_ptr<Value> Get (size_t index) override;
        INT_TYPE Len () override;
        void Serialize (std::ostream& stream) override;
        void Set (size_t index, std::shared_ptr<Value> to) override;
        std::shared_ptr<Value>& operator[] (size_t index) override;
        static std::shared_ptr<Value> New ();
    };
    struct Obj : public Value
    {
        ValueType Type () override;
        std::unordered_map<std::string, std::shared_ptr<Value>> values;
        std::string AsStr () override;
        std::shared_ptr<Value> Get (std::string key) override;
        std::vector<std::string> Keys () override;
        bool Has (std::string key) override;
        void Serialize (std::ostream& stream) override;
        void Set (std::string key, std::shared_ptr<Value> to) override;
        std::shared_ptr<Value>& operator[] (std::string key) override;
        static std::shared_ptr<Value> New ();
    };
}
