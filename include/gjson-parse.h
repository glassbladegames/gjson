#pragma once

#include "gjson-cpp.h"
#include "char-source.h"

namespace json
{
	struct ParseException : public GeneralException {};
	struct BadSyntax : public ParseException
    {
        int line, col;
        std::string why;
        BadSyntax (int line, int col, std::string why)
            : line(line), col(col), why(why)
        {}
    };
	std::shared_ptr<Value> Parse (CharSource* char_src);
}
